//
//  PicturesListModel.h
//  DailyDiary
//
//  Created by mob on 2019/9/26.
//  Copyright © 2019 howhyone. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PicturesListModel : NSObject
@property(nonatomic, strong)NSString *num;
@property(nonatomic, strong)NSString *src;
@property(nonatomic, strong)NSString *date;

@end

NS_ASSUME_NONNULL_END
