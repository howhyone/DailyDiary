//
//  DD_RootViewController.h
//  DailyDiary
//
//  Created by mob on 2019/8/17.
//  Copyright © 2019 howhyone. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DD_RootViewController : UIViewController

-(void)showOpenScreenAD;

@end

NS_ASSUME_NONNULL_END
