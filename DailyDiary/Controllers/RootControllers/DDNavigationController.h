//
//  DDNavigationController.h
//  DailyDiary
//
//  Created by mob on 2019/8/17.
//  Copyright © 2019 howhyone. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DDNavigationController : UINavigationController

@end

NS_ASSUME_NONNULL_END
