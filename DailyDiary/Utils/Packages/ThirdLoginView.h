//
//  ThirdLoginView.h
//  DailyDiary
//
//  Created by mob on 2019/8/24.
//  Copyright © 2019 howhyone. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ThirdLoginView : UIView


@property(nonatomic, strong)UIButton *wechatBtn;
@property(nonatomic, strong)UIButton *sinaBtn;

@end

NS_ASSUME_NONNULL_END
