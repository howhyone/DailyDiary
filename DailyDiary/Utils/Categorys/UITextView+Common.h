//
//  UITextView+Common.h
//  DailyDiary
//
//  Created by mob on 2019/9/22.
//  Copyright © 2019 howhyone. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextView (Common)

+(UITextView *)textViewWithFontSize:(CGFloat)fontSize WithFontColor:(UInt32)fontColor;


@end

NS_ASSUME_NONNULL_END
