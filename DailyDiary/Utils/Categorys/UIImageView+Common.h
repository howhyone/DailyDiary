//
//  UIImageView+Common.h
//  DailyDiary
//
//  Created by mob on 2019/8/22.
//  Copyright © 2019 howhyone. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (Common)
+(UIImageView *)imageViewWithImageName:(NSString *)imageNameStr;
@end

NS_ASSUME_NONNULL_END
